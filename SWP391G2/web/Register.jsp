<%-- 
    Document   : Register
    Created on : Jan 15, 2022, 2:51:46 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="resource/register.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <form action="registration" method="post">
            <div class="container">
                <h1>Create Account</h1>
                <p>Enter your information below to register</p>
                <hr>
                
                <label for="fullname"><b>Full Name</b></label>
                    <input type="text" placeholder="Enter Full Name" name="fullname1" required>
                
                <div style="margin-bottom: 22px;">
                <label for="gender"><b>Gender</b></label>
                <input style="margin-left: 20px;" type="radio" name="gender1" value="male"/><b style="color:DodgerBlue;">Male</b>
                <input style="margin-left: 20px;" type="radio" name="gender1" value="female"/><b style="color:DodgerBlue">Female</b><br>
                </div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email1" required>
                
                <label for="phone"><b>Phone Number</b></label>
                <input type="text" placeholder="Enter Phone Number" name="phonenumber1" required>
                
                <label for="username"><b>User Name</b></label>
                <input type="text" placeholder="Enter User Name" name="username1" required>

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw1" required>

<!--                <label for="psw-repeat"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw-repeat1" required>

                <label>
                    <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Nhớ Đăng Nhập
                </label>-->

                <div class="clearfix">
                    <button type="submit" class="signupbtn">Sign Up</button>
                    <!--<button type="cancel" class="cancelbtn">Cancel</button>-->
                </div>
            </div>
        </form>
    </body>
</html>
