/*
 * Xuan Son
 * 
 * Jan 17, 2022
 *
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.Account;

/**
 *
 * @author Xuan Son
 */
public class AccountDAO extends dbcontext.dbContext {

    public void signup(Account ac) {
        try {
            String sql = "insert into account(fullname,gender,email,phonenumber,username,password)\n"
                    + "values(?,?,?,?,?,?)";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, ac.getFullName());
            ps.setInt(2, ac.getGender());
            ps.setString(3, ac.getEmail());
            ps.setString(4, ac.getPhoneNumber());
            ps.setString(5, ac.getUsername());
            ps.setString(6, ac.getPassword());
            ps.executeUpdate();

        } catch (SQLException e) {
             e.printStackTrace(System.out);
        }
    }
    
    public static void main(String[] args) {
        Account account = new Account();
        account.setFullName("Vu Quang");
        account.setGender(1);
        account.setEmail("vuquang@gmail.com");
        account.setPhoneNumber("0914583889");
        account.setUsername("vugquang1");
        account.setPassword("vuquang1");
        AccountDAO accountDAO = new AccountDAO();
        accountDAO.signup(account);
        System.out.println("ok");
    }
}
